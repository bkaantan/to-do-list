package info.androidhive.todolist;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import java.util.ArrayList;



public class DatabaseHandler  extends SQLiteOpenHelper {

    public DatabaseHandler(Context applicationcontext) {
        super(applicationcontext, "todolist.db", null, 1);
    }
    //Creates Table
    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL("CREATE TABLE task ( id INTEGER PRIMARY KEY, title TEXT, desc TEXT, date TEXT,priority TEXT,conName TEXT,conNumber TEXT)");

    }
    @Override
    public void onUpgrade(SQLiteDatabase database, int version_old, int current_version) {
        String query;
        query = "DROP TABLE IF EXISTS task";
        database.execSQL(query);

        onCreate(database);
    }

    public void resetTables(){
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            // Delete All Tables And Rows
            db.delete("task", null, null);


            if ( db.isOpen())
                db.close();
        } catch (Exception e) {
            Log.e("Error Database ", e.getMessage());
        }

    }

    public void insertTask(Task task) {
        //String timeStamp = new SimpleDateFormat("yyyy-MM-dd- HH:mm:ss").format(new Date());
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("id",task.getId());
        values.put("title",task.getTitle());
        values.put("desc",task.getDescription());
        values.put("date",task.getDate());
        values.put("priority",task.getPriority());
        values.put("conName",task.getConName());
        values.put("conNumber",task.getConNumber());
        database.insert("task", null, values);
        database.close();
    }

    public ArrayList<Task> getAllTask()
    {
        ArrayList<Task> arrayListTask = new  ArrayList<Task>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery("select * from task", null);
        res.moveToFirst();
        Task n;
        while(res.isAfterLast() == false){
            n = new Task();
            n.setId(res.getInt(res.getColumnIndex("id")));
            n.setTitle(res.getString(res.getColumnIndex("title")));
            n.setDescription(res.getString(res.getColumnIndex("desc")));
            n.setPriority(res.getString(res.getColumnIndex("priority")));
            n.setDate(res.getString(res.getColumnIndex("date")));
            n.setConName(res.getString(res.getColumnIndex("conName")));
            n.setConNumber(res.getString(res.getColumnIndex("conNumber")));
            arrayListTask.add(n);
            res.moveToNext();
        }
        db.close(); // Closing database connection
        return arrayListTask;
    }

    public boolean deleteTitle(int id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("task", "id" + "=" + id, null) > 0;
    }
}