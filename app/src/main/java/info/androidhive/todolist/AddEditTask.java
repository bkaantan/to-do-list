package info.androidhive.todolist;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AddEditTask extends AppCompatActivity implements View.OnClickListener {
    Calendar myCalendar;
    DatePickerDialog.OnDateSetListener date;
    private EditText title,desc,deadline,conName,conNumber;
    private Spinner priority;
    Button clear,save,back,choose;
    ArrayList<String> priorityList;
    DatabaseHandler dbOps;
    private ProgressBar bar;
    private static final int REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit_task);
        myCalendar = Calendar.getInstance();
        deadline=(EditText) findViewById(R.id.deadline);
        title=(EditText) findViewById(R.id.titleText);
        desc=(EditText) findViewById(R.id.descrpText);
        conName=(EditText) findViewById(R.id.conName);
        conNumber=(EditText) findViewById(R.id.conNumber);
        priority = (Spinner) findViewById(R.id.prioritySpinner);
        clear = (Button) findViewById(R.id.clearButton);
        save = (Button) findViewById(R.id.saveButton);
        choose = (Button) findViewById(R.id.chooseButton);
        back = (Button) findViewById(R.id.backButton);
        bar = (ProgressBar) findViewById(R.id.progressBar);
        myCalendar = Calendar.getInstance();
        dbOps=new DatabaseHandler(this);
        setListeners();
        priorityList=new ArrayList<String>();
        priorityList.add(0,"Low");
        priorityList.add(1,"Normal");
        priorityList.add(2,"High");

        ArrayAdapter<String> adapter_state = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item, priorityList);
        priority.setAdapter(adapter_state);


        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        deadline.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(AddEditTask.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent intent) {
        if (requestCode == REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Uri uri = intent.getData();
                String[] projection = { ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME };

                Cursor cursor = getContentResolver().query(uri, projection,
                        null, null, null);
                cursor.moveToFirst();

                int numberColumnIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                String number = cursor.getString(numberColumnIndex);

                int nameColumnIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
                String name = cursor.getString(nameColumnIndex);

                Log.d("contact", "ZZZ number : " + number +" , name : "+name);

                conName.setText(name);
                conNumber.setText(number);
            }
        }
    };

    private void setListeners() {
        save.setOnClickListener(this);
        clear.setOnClickListener(this);
        back.setOnClickListener(this);
        choose.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.saveButton:
                checkValidation();
                break;

            case R.id.clearButton:
                title.setText("");
                desc.setText("");
                deadline.setText("");
                conName.setText("");
                conNumber.setText("");
                break;


            case R.id.backButton:
                onBackPressed();
                break;

            case R.id.chooseButton:
                Uri uri = Uri.parse("content://contacts");
                Intent intent = new Intent(Intent.ACTION_PICK, uri);
                intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                startActivityForResult(intent, REQUEST_CODE);
                break;

        }
    }


    private void updateLabel() {

        String myFormat = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.UK);

        deadline.setText(sdf.format(myCalendar.getTime()));
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        finish();
    }

    private void checkValidation() {

        // Get all edittext texts
        String gettitle = title.getText().toString();
        String getdesc = desc.getText().toString();
        String getpriority = priority.getSelectedItem().toString();
        String getdeadline = deadline.getText().toString();
        String getnum=conNumber.getText().toString();
        String getnam=conName.getText().toString();

        // Check if all strings are null or not
        if (    gettitle.equals("") || gettitle.length() == 0
                || getdesc.equals("") || getdesc.length() == 0
                || getpriority.equals("") || getpriority.length() == 0
                || getdeadline.equals("") || getdeadline.length() == 0){

            Toast.makeText(getApplicationContext(),
                    "All fields are required.",
                    Toast.LENGTH_LONG).show();
        }

        else {

            new ProgressTask().execute(gettitle,getdesc,getpriority,getdeadline,getnum,getnam);

        }




        }

    private class ProgressTask extends AsyncTask<String,Void,String> {

        @Override
        protected void onPreExecute(){
            bar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String...params) {

            String tit=params[0];
            String des=params[1];
            String pri=params[2];
            String dead=params[3];
            String num=params[4];
            String nam=params[5];
                try{
                    Task t = new Task();
                    Random r = new Random();
                    int i1 = r.nextInt(10000 - 2 + 1) + 2;
                    t.setId(i1);
                    t.setTitle(tit);
                    t.setDescription(des);
                    t.setPriority(pri);
                    t.setDate(dead);
                    t.setConName(nam);
                    t.setConNumber(num);
                    dbOps.insertTask(t);
                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    return "001";
                }catch (Exception e){
                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException b) {
                        e.printStackTrace();
                    }
                    return "002";
                }



        }

        @Override
        protected void onPostExecute(String s) {
            bar.setVisibility(View.GONE);
            if (s.equals("001")) {
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            } else {
                Toast.makeText(getApplicationContext(),
                        "Error!",
                        Toast.LENGTH_LONG).show();
            }
        }
    }
    }



