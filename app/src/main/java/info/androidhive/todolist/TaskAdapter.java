package info.androidhive.todolist;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.List;


public class TaskAdapter extends RecyclerView.Adapter<TaskAdapter.MyViewHolder> {

    private Context mContext;
    private List<Task> taskList;
    DatabaseHandler dbOps=new DatabaseHandler(Contexter.getContext());
    int a=-1;
    Task c;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, count;
        public ImageView thumbnail, overflow;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            count = (TextView) view.findViewById(R.id.count); //date
            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            overflow = (ImageView) view.findViewById(R.id.overflow);
        }
    }


    public TaskAdapter(Context mContext, List<Task> taskList) {
        this.mContext = mContext;
        this.taskList = taskList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.task_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        Task task = taskList.get(position);
        c=taskList.get(position);
        holder.title.setText(task.getTitle());
        holder.count.setText(task.getDate());
        a=task.getId();
        String pri=task.getPriority();
        if(pri.equals("High")){
            Glide.with(mContext).load(R.drawable.p3).into(holder.thumbnail);
        }
        else if(pri.equals("Normal")){
            Glide.with(mContext).load(R.drawable.p2).into(holder.thumbnail);
        }
        else if(pri.equals("Low")){
            Glide.with(mContext).load(R.drawable.p1).into(holder.thumbnail);
        }
        else{
            Glide.with(mContext).load(R.drawable.p4).into(holder.thumbnail);
        }
        // loading task cover using Glide library


        holder.overflow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopupMenu(holder.overflow,taskList.get(position));
            }
        });
    }

    /**
     * Showing popup menu when tapping on 3 dots
     */
    private void showPopupMenu(View view,Task b) {
        // inflate menu
        PopupMenu popup = new PopupMenu(mContext, view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_album, popup.getMenu());
        popup.setOnMenuItemClickListener(new MyMenuItemClickListener(b));
        popup.show();
    }

    /**
     * Click listener for popup menu items
     */
    class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {
        Task g;

        public MyMenuItemClickListener(Task g) {
            this.g = g;
        }

        public MyMenuItemClickListener() {
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.action_show:
                   showTask(g.getTitle(),g.getDescription(),g.getPriority(),g.getDate(),g.getConName(),g.getConNumber());
                    return true;
                case R.id.action_delete:
                    dbOps.deleteTitle(g.getId());
                    MainActivity.taskList = dbOps.getAllTask();
                    MainActivity.adapter = new TaskAdapter(Contexter.getContext(), MainActivity.taskList);
                    MainActivity.recyclerView.setAdapter(MainActivity.adapter);
                    MainActivity.adapter.notifyDataSetChanged();
                    return true;
                case R.id.action_send:
                    Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                    sendIntent.putExtra("sms_body", "Title:"+g.getTitle()+"\n"+"Description: "+g.getDescription()+"\n"+"Priority:"+
                    g.getPriority()+"\n"+"Deadline:"+g.getDate());
                    sendIntent.setType("vnd.android-dir/mms-sms");
                    Contexter.getContext().startActivity(sendIntent);
                    return true;
                default:
            }
            return false;
        }
    }

    @Override
    public int getItemCount() {
        return taskList.size();
    }

    public void showTask(String title,String desc,String pri,String deadline,String name,String num){

        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(Contexter.getContext());
        View promptsView = li.inflate(R.layout.popup, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                Contexter.getContext());

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final TextView t = (TextView) promptsView
                .findViewById(R.id.textView2);
        final TextView t1 = (TextView) promptsView
                .findViewById(R.id.textView4);
        final TextView t2 = (TextView) promptsView
                .findViewById(R.id.textView6);
        final TextView t3 = (TextView) promptsView
                .findViewById(R.id.textView8);
        final TextView t4 = (TextView) promptsView
                .findViewById(R.id.textView10);

        t.setText(title);
        t1.setText(desc);
        t2.setText(pri);
        t3.setText(deadline);
        t4.setText(name+" "+num);

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();


    }
}
